///////////////////////////////////////////////////////////////////////////////
// TOOLS
#tool NUnit.Extension.TeamCityEventListener&version=1.0.6
#tool "nuget:?package=NUnit.ConsoleRunner"
///////////////////////////////////////////////////////////////////////////////
// ARGUMENTS
var path = Argument("path","MyCalculator");
var solution = Argument("solution",$"./{path}/{path}.sln");
var configuration = Argument("configuration","Release");
var target = Argument("target", "Default");
///////////////////////////////////////////////////////////////////////////////
// SETUP / TEARDOWN
///////////////////////////////////////////////////////////////////////////////

Setup(ctx =>
{
   // Executed BEFORE the first task.
   Information("Running tasks...");
});

Teardown(ctx =>
{
   // Executed AFTER the last task.
   Information("Finished running tasks.");
});

TaskSetup(setupContext =>
{
   if(TeamCity.IsRunningOnTeamCity)
   {
      TeamCity.WriteStartBuildBlock(setupContext.Task.Description ?? setupContext.Task.Name);

      TeamCity.WriteStartProgress(setupContext.Task.Description ?? setupContext.Task.Name);
   }
});

TaskTeardown(teardownContext =>
{
   if(TeamCity.IsRunningOnTeamCity)
   {
      TeamCity.WriteEndProgress(teardownContext.Task.Description ?? teardownContext.Task.Name);

      TeamCity.WriteEndBuildBlock(teardownContext.Task.Description ?? teardownContext.Task.Name);
   }
});
///////////////////////////////////////////////////////////////////////////////
// TASKS
///////////////////////////////////////////////////////////////////////////////n

Task("Restore-NuGet-Packages")
.Description("Restore NuGet packages")
.Does(() => {
   NuGetRestore(solution, new NuGetRestoreSettings{Source = new List<string>{"https://www.nuget.org/api/v2"}});
});

Task("Build")
.Description("Build project")
.IsDependentOn("Restore-NuGet-Packages")
.Does(() => {  
   MSBuild(solution, settings =>
      settings
         .SetConfiguration(configuration)
         .SetVerbosity(Verbosity.Minimal)
         .WithProperty("VisualStudioVersion", new string[]{"16.0"}));
         //.UseToolVersion(MSBuildToolVersion.VS2019));
         });
Task("RunTests")
.Description("Run tests")
.IsDependentOn("Build")
.Does(() => {
    var testDllsPattern = string.Format("*/*/bin/{0}/*.Test.dll", configuration);
    NUnit3(testDllsPattern, new NUnit3Settings()
    {
      NoResults = true     
    });
});
//////////////////////////////////////////////////////////////////////
// TASK TARGETS
//////////////////////////////////////////////////////////////////////
Task("Default")
.IsDependentOn("Restore-NuGet-Packages")
.IsDependentOn("Build")
.IsDependentOn("RunTests");
//////////////////////////////////////////////////////////////////////
// EXECUTION
//////////////////////////////////////////////////////////////////////
RunTarget(target);