---
marp: true
theme: gaia
_class: lead
paginate: true
backgroundColor: #fff
backgroundImage: url('https://marp.app/assets/hero-background.jpg')
//header: 'Header content'
footer: 'Командная разработка программ - Грищук Кирилл'
---

# <!-- fit --> Командная разработка программ
\
\
**<!-- fit -->Грищук Кирилл**

---

# <!-- fit --> Процесс разработки
![width:150px width:150px](./pic/team.svg)
Смена фокуса разработки от одиночек к командам 
![width:200px width:150px](./pic/interaction.svg)
Взамодействие людей в команде
<!-- -->

---
# Проблемы

- ###  Синхронизация работы в команде
- ### Обеспечение качества выпускаемых программ
- ### Скорость выпуска программ
---

# Git
- Решение проблемы синхронизации работы в команде
![width:150px width:150px](./pic/git.png)
- Репозиторий
![height:330](./pic/gitlog.png)

---
# <!-- fit --> Непрерывная интеграция
- Гарантия качества выпускаемых продуктов
![width:700 height:500](./pic/cicd.png)

---

# Механизмы непрерывной интеграции
1. Сборка
2. Модульное тестирование
3. Публикация
4. Интеграционное тестирование
5. Выпуск релиза

---

# Средства непрерывной интеграции

![width:300](./pic/jenkins.png)       ![width:300](./pic/teamcity.png)

---
# Свой сценарий без сложностей
![width:250](./pic/cake.png) 

```csharp
Task("Build")
.Description("Build project")
.IsDependentOn("Restore-NuGet-Packages")
.Does(() => { /* Any actions */ }); 
```

---
# Попробуем

1. Открыть проводник
2. Перейти в папку "Документы"
3. Перейти в каталог "hse-winter-school"
4. В меню пуск запустить cmd.exe

---
# Повторяем

---

#  Конец
## Спасибо за внимание
## Готов ответить на ваши вопросы