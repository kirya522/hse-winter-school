---
marp: true
theme: gaia
_class: lead
paginate: true
backgroundColor: #fff
backgroundImage: url('https://marp.app/assets/hero-background.jpg')
---

# Командная разработка программ
\
\
**Грищук Кирилл**

---

# Процесс разработки

![width:300px width:200px](./pic/team.svg)
Смена фокуса от одиночек к командам 


---

# Git
![50% center](./pic/git.png)

---

# Непрерывная интеграция

---

# Сценарии непрерывной интеграции

---

# Попробуем сами
1. Запускаем bash эмулятор
2. Клонируем репозиторий
```
git clone https://github.com/Kirya522/hse-winter-school
```
2. Открываем Visual studio

---


# End

## Thanks for watching