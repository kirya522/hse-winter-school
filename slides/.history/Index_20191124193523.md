---
marp: true
theme: uncover
_class: lead
paginate: true
backgroundColor: #fff
backgroundImage: url('https://marp.app/assets/hero-background.jpg')
---

# Командная разработка ПО, сценарии непрерывной интеграции
### Грищук Кирилл

---

# Командная разработка

---

# Git

---

# Непрерывная интеграция

---

# Сценарии непрерывной интеграции

---

# Попробуем сами
1. Запускаем bash эмулятор
2. Клонируем репозиторий
```
git clone https://github.com/Kirya522/hse-winter-school
```
2. Открываем Visual studio

---


# End

## Thanks for watching