﻿using System;
using NUnit.Framework;

namespace MyCalculator.Test
{
    [TestFixture]
    public class UnitTest1
    {
        [Test]
        [TestCase(1,2,3)]
        [TestCase(4,2,6)]
        [TestCase(1,99,100)]
        public void CanAddTwoItems(double a, double b, double res)
        {
            var provided = MyForms.Classes.MyCalculator.Add(a, b);
            Assert.AreEqual(provided, res, 0.00001d);
        }
    }
}
