﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MyForms.Classes;

namespace MyForms
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void calculateButton_Click(object sender, EventArgs e)
        {
            if (double.TryParse(firstNumberTextBox.Text, out var doubleVal1) &&
                double.TryParse(secondNumberTextBox.Text, out var doubleVal2))
            {
                var res = MyCalculator.Add(doubleVal1, doubleVal2);
                resultTextBox.Text = res.ToString();
            }
        }
    }
}
