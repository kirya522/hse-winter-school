﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyForms.Classes
{
    public static class MyCalculator
    {
        public static double Add(double a, double b)
        {
            return a + b;
        }
    }
}
